$(document).ready(function () {

    // Global

    $('a').on("click", function (e) {
        if ($(this).attr('href') == '') {
            e.preventDefault();
        } else {
            return true;
        }
    });
    //плавный скролл между якорями
    $('.smoothScroll').click(function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var target = $(href);
        var top = target.offset().top;
        $('html,body').animate({scrollTop: top}, 1000);
    });



    //Кнопка вверх
    var scrollUp = document.getElementById('scrollup');
    scrollUp.onclick = function () {
        jQuery('html, body').animate({scrollTop: 0}, 500);
        return false;
    };

    window.onscroll = function () {
        if (window.pageYOffset > 0) {
            scrollUp.style.display = 'block';
        } else {
            scrollUp.style.display = 'none';
        }
    };


    $('.first-carousel').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
         items: 1
        // autoplay: true,
        // autoplayTimeout: 5000
    });

    $('.goods-carousel').owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        dots: false,
        items: 4,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive: {

            0: {},
            768: { }
        }
    });


    $('.owl-prev').html('');
    $('.owl-next').html('');






});

